package com.example.useretrofittocallapi.weather_api;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.useretrofittocallapi.R;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WeatherFrg extends Fragment {
    private EditText editText;
    private Button button;
    private TextView responseText;

    @Nullable
    @Override
    public View onCreateView(@androidx.annotation.NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frg_weather_api, container, false);
        init(v);
        return v;
    }

    private void init(View view) {
        editText = view.findViewById(R.id.edt_id);
        button = view.findViewById(R.id.bt_find);
        responseText = view.findViewById(R.id.tv_rs);
        button.setOnClickListener(v -> {
//                fetchWeatherDetails();
            getWeatherInfo(editText.getText().toString());
        });
    }

    private void getWeatherInfo(String name) {
        Retrofit retrofit = NetworkClient.getRetrofitClient();

        WeatherAPIs weatherAPIs = retrofit.create(WeatherAPIs.class);

        weatherAPIs.getWeatherByCityByRx(name, "b6907d289e10d714a6e88b30761fae22")
                .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<WRespone>() {
                @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull WRespone wRespone) {
                responseText.setText("Temp: " + wRespone.getMain().getTemp());
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void fetchWeatherDetails() {

        Retrofit retrofit = NetworkClient.getRetrofitClient();

        WeatherAPIs weatherAPIs = retrofit.create(WeatherAPIs.class);

        Call call = weatherAPIs.getWeatherByCity(editText.getText().toString(), "b6907d289e10d714a6e88b30761fae22");

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null) {
                    WRespone item = (WRespone) response.body();
                    responseText.setText("Temp: " + item.getMain().getTemp());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });

//        Call<ResponseBody> call = weatherAPIs.getWeatherByCity(editText.getText().toString(), "b6907d289e10d714a6e88b30761fae22");
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if(response.body()!= null){
//                    Object  jsonObject = null;
//                    try {
//                        jsonObject = new JSONTokener(response.body().toString()).nextValue();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    responseText.setText(jsonObject + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Throwable t) {
//
//            }
//        });

    }
}
