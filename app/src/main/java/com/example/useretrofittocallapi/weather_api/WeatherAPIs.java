package com.example.useretrofittocallapi.weather_api;

import com.example.useretrofittocallapi.weather_api.WRespone;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherAPIs {
    @GET("/data/2.5/weather")
    Call<WRespone> getWeatherByCity(@Query("q") String city, @Query("appid") String apiKey);

    @GET("/data/2.5/weather")
    Observable<WRespone> getWeatherByCityByRx(@Query("q") String city, @Query("appid") String apiKey);

}
