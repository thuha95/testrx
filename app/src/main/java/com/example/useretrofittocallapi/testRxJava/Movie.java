package com.example.useretrofittocallapi.testRxJava;

public class Movie {
    public String name;
    public String detail;

    public Movie(String name, String detail) {
        this.name = name;
        this.detail = detail;
    }
}
