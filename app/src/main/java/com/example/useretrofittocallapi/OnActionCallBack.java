package com.example.useretrofittocallapi;

import androidx.fragment.app.Fragment;

public interface OnActionCallBack {
    void showFragment(Fragment fragment, String name);
}
