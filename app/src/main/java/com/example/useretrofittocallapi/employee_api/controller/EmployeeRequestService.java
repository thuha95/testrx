package com.example.useretrofittocallapi.employee_api.controller;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmployeeRequestService {
    private static final String BASE_URL = "http://dummy.restapiexample.com";
    public static Retrofit instance;

    public static Retrofit getInstance() {
        if (instance == null) {
            instance = new Retrofit.Builder().baseUrl(BASE_URL).client(new OkHttpClient.Builder().build()).build();
        }
        return instance;
    }

    public static Retrofit getInstanceUseGson() {
        if (instance == null) {
            instance = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(new OkHttpClient.Builder().build()).build();
        }
        return instance;
    }
}
