package com.example.useretrofittocallapi.employee_api.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.useretrofittocallapi.R;
import com.example.useretrofittocallapi.employee_api.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.VH> {
    private List<Employee> listData = new ArrayList<>();
    private Context mContext;

    public EmployeeAdapter(List<Employee> listData) {
        this.listData = listData;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_test, parent, false);
        mContext = parent.getContext();
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        Employee item = listData.get(position);
        holder.tvId.setText(String.format("Id: %s", item.getId()));
        holder.tvAge.setText(String.format("Age: %s", item.getAge()));
        holder.tvName.setText(String.format("Name: %s", item.getName()));
        holder.tvSalary.setText(String.format("Salary: %s", item.getSalary()));
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvId, tvName, tvAge, tvSalary;
        public VH(@NonNull View itemView) {
            super(itemView);
            tvAge = itemView.findViewById(R.id.tv_age);
            tvId = itemView.findViewById(R.id.tv_id);
            tvSalary = itemView.findViewById(R.id.tv_salary);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }
}
