package com.example.useretrofittocallapi.employee_api;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.useretrofittocallapi.ControllerCallBack;
import com.example.useretrofittocallapi.R;
import com.example.useretrofittocallapi.employee_api.adapter.EmployeeAdapter;
import com.example.useretrofittocallapi.employee_api.controller.EmployeeController;
import com.example.useretrofittocallapi.employee_api.model.Employee;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class EmployeeFragment extends Fragment {
    private RecyclerView rvDataEmployee;
    private EditText edtSearch;
    private EmployeeController employeeController;
    private List<Employee> listData = new ArrayList<>();
    private EmployeeAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frg_employee, container, false);
        initViews(v);
        getData();
        return v;
    }

    private void getData() {
        getEmployeeController().getAllEmployee(new ControllerCallBack(){
            @Override
            public void onFinish(Object object) {
                if(object instanceof List){
                    listData = (List) object;
                    adapter = new EmployeeAdapter(listData);
                    rvDataEmployee.setAdapter(adapter);
                }
                super.onFinish(object);
            }

            @Override
            public void onFailure(Object obj) {
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                super.onFailure(obj);
            }
        });
    }

    private EmployeeController getEmployeeController() {
        if(employeeController == null){
            employeeController = new EmployeeController();
        }
        return employeeController;
    }

    private boolean isSearch;
    private void initViews(View v) {
        rvDataEmployee = v.findViewById(R.id.rv_data_employee);
        rvDataEmployee.setLayoutManager(new LinearLayoutManager(getContext()));
        v.findViewById(R.id.imv_search).setOnClickListener(view -> {
            if(isSearch){
                isSearch = false;
                edtSearch.setVisibility(View.GONE);
            } else {
                edtSearch.setVisibility(View.VISIBLE);
                isSearch = true;
            }
        });
        edtSearch = v.findViewById(R.id.edt_search);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            private final long DELAY = 500; // milliseconds


            Handler handler = new Handler(Looper.getMainLooper() /*UI thread*/);

            @Override
            public void afterTextChanged(final Editable s) {
                handler.removeCallbacks(workRunnable);
                if (s == null) {
                    adapter = new EmployeeAdapter(listData);
                    rvDataEmployee.setAdapter(adapter);
                    return;
                }
                if (s.toString().equals("")) {
                    adapter = new EmployeeAdapter(listData);
                    rvDataEmployee.setAdapter(adapter);
                    return;
                }

//                createNewThread(s);
                workRunnable = () -> {
                    Log.i("Observable 1", Thread.currentThread().getName() );
                    doSearch(s.toString());
                };
                handler.postDelayed(workRunnable, DELAY /*delay*/);
            }
        });
    }

    Runnable workRunnable;
    private void createNewThread(Editable s) {
        workRunnable = () -> {
            Log.i("Observable 1", Thread.currentThread().getName() );
            doSearch(s.toString());
        };
        Thread thread = new Thread(workRunnable);
        thread.start();
    }

    private void doSearch(String toString) {
        Observable.fromIterable(listData).filter(employee -> {
            Log.i("Observable 2", Thread.currentThread().getName() );
            return (employee.getId() + "").contains(toString);
        }).toList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(employees -> {
            adapter = new EmployeeAdapter(employees);
            rvDataEmployee.setAdapter(adapter);
        });

    }



}
