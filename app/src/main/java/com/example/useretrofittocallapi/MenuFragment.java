package com.example.useretrofittocallapi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.useretrofittocallapi.OnActionCallBack;
import com.example.useretrofittocallapi.R;
import com.example.useretrofittocallapi.RenderAdapter.RenderFragment;
import com.example.useretrofittocallapi.api_ghtk.ui.LoginFragment;
import com.example.useretrofittocallapi.employee_api.EmployeeFragment;
import com.example.useretrofittocallapi.testRxJava.RxJavaFragment;
import com.example.useretrofittocallapi.weather_api.WeatherFrg;

public class MenuFragment extends Fragment implements View.OnClickListener {
    private OnActionCallBack mCallBack;

    public void setmCallBack(OnActionCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frg_menu, container, false);
        initViews (v);
        return v;
    }

    private void initViews(View v) {
        v.findViewById(R.id.bt_ghtk_api).setOnClickListener(this);
        v.findViewById(R.id.bt_render_adpater).setOnClickListener(this);
        v.findViewById(R.id.bt_rxjava).setOnClickListener(this);
        v.findViewById(R.id.bt_employee_api).setOnClickListener(this);
        v.findViewById(R.id.bt_weather_api).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_employee_api:
                EmployeeFragment employeeFragment = new EmployeeFragment();
                mCallBack.showFragment(employeeFragment, EmployeeFragment.class.getName());
                break;
            case R.id.bt_render_adpater:
                RenderFragment renderFragment = new RenderFragment();
                mCallBack.showFragment(renderFragment, RenderFragment.class.getName());
                break;
            case R.id.bt_rxjava:
                RxJavaFragment rxJavaFragment = new RxJavaFragment();
                mCallBack.showFragment(rxJavaFragment, RxJavaFragment.class.getName());
                break;
            case R.id.bt_weather_api:
                WeatherFrg weatherFrg = new WeatherFrg();
                mCallBack.showFragment(weatherFrg, WeatherFrg.class.getName());
                break;
            case R.id.bt_ghtk_api:
                LoginFragment loginFrg = new LoginFragment();
                mCallBack.showFragment(loginFrg, LoginFragment.class.getName());
                break;
        }
    }
}
