package com.example.useretrofittocallapi.api_ghtk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.useretrofittocallapi.R;

import java.util.ArrayList;
import java.util.List;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.VH> {
    private Context mContext;
    private List<String> mListData = new ArrayList<>();
    private int counMax = 5;

    public TestAdapter(Context mContext, List<String> mListData) {
        this.mContext = mContext;
        this.mListData = mListData;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_test, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull  VH holder,  int position) {
        final int pos = position;
        final int[] lineCount = new int[1];

        String item = mListData.get(position);
//        holder.tvTest.setText(item);

//        final TextView tv = holder.tvTest;
//        tv.post(new Runnable() {
//            @Override
//            public void run() {
//                int lines = tv.getLineCount();
//                lineCount[0] = lines;
//                setSth(pos,lineCount[0],t);
//                Log.i("TAGGGG", pos + " lineCount " + lineCount[0]);
//            }
//        });

//        if (position == 5) {
//            holder.tvTest.setText(customTextOwn(lineCount[0], item));
//        } else {
//            if (lineCount[0] >= counMax) {
//                holder.tvTest.setText(customList(position, lineCount[0], item));
//            } else {
//                counMax = counMax - lineCount[0];
//            }
//        }


    }

    private void setSth(int pos, int i) {
    }

    private String customList(int position, int lineCount, String item) {
        for (int i = 0; i < position; i++) {
            mListData.remove(i);
        }
        notifyDataSetChanged();
        if (lineCount == counMax) {
            return item;
        } else {
            int numChaInLine = item.length() / lineCount;
            int index = ((lineCount - counMax) / lineCount) * numChaInLine;
            return "..." + item.substring(index, item.length() - 1);
        }

    }

    private String customTextOwn(int lineOwnCount, String text) {
        if (lineOwnCount == 1) {
            return text;
        } else {
            int numChaInLine = text.length() / lineOwnCount;
            int index = ((lineOwnCount - 1) / lineOwnCount) * numChaInLine;
            return "..." + text.substring(index, text.length() - 1);
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        private TextView tvTest;

        public VH(@NonNull View itemView) {
            super(itemView);
//            tvTest = itemView.findViewById(R.id.tvTest);

        }
    }
}
