package com.example.useretrofittocallapi.api_ghtk;

public final class BuildConfig1 {
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "vn.ghtk.carrier.stage";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "stage";
    public static final int VERSION_CODE = 1511;
    public static final String VERSION_NAME = "7.5.511";
}

