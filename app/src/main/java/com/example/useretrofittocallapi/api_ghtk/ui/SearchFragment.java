package com.example.useretrofittocallapi.api_ghtk.ui;

import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.useretrofittocallapi.R;
import com.example.useretrofittocallapi.api_ghtk.Controller.RequestAPI;
import com.example.useretrofittocallapi.api_ghtk.Controller.RequestService;
import com.example.useretrofittocallapi.api_ghtk.adapter.TestAdapter;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SearchFragment extends Fragment {
    private EditText edtId;
    private TextView tvRs;
    private RecyclerView rvTest;
    private TestAdapter adapter;
    private Retrofit retrofit = RequestService.getInstance();
    String path = "admin/packages/searchOrderByUser?term=";
    String cookie = "PHPSESSID=";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frg_search, container, false);
        initView(v);
        return v;
    }

    private void initView(View v) {
        edtId = v.findViewById(R.id.edt_id);
        tvRs = v.findViewById(R.id.tv_rs);
        TextPaint paint = tvRs.getPaint();
        int wordwidth=(int)paint.measureText("a",0,1);
        int screenwidth = getResources().getDisplayMetrics().widthPixels;
        int num = screenwidth/wordwidth;
        Toast.makeText(getActivity(), num + "", Toast.LENGTH_SHORT).show();
        tvRs.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = tvRs.getLineCount();
                Toast.makeText(getContext(),lineCount +"",Toast.LENGTH_SHORT).show();
            }
        });


        v.findViewById(R.id.bt_find).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSearch();
            }
        });

//        rvTest = v.findViewById(R.id.rv_test);
//        String[] strings = new String[]{"AAAAAAAAAAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAABBBBBBBBBBBBBBBVVVVVVVVVVVFFFFFFFFDDDDĐ"};
//        adapter = new TestAdapter(getContext(), Arrays.asList(strings));
//        rvTest.setLayoutManager(new LinearLayoutManager(getContext()));
//        rvTest.setAdapter(adapter);

    }

    private void doSearch() {
        getRequestAPT().doGetWithId(path + edtId.getText().toString(), cookie + "u8dojm2d1u8ae6r3pahtucua11").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject post = null;
                try {
                    post = (JSONObject) new JSONObject().get(response.body().toString());
                    if (post != null) {
                        Log.i("TAGGG", "có obj");
//                        JSONArray array = post.getJSONArray("data");
//                        JSONObject obj = array.getJSONObject(0);
//                        JSONObject item = obj.getJSONObject("Package");
//                        if (item.has("message") && !obj.isNull("message")) {
//                            String text = obj.getString("message");
//                            tvRs.setText(text);
//                        }
                    } else {
                        tvRs.setText("Không có obj");
                        Toast.makeText(getContext(), "nullllll", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("@@@", "error");
                Toast.makeText(getContext(), "không load được", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private RequestAPI getRequestAPT() {
        return retrofit.create(RequestAPI.class);
    }
}
