package com.example.useretrofittocallapi.api_ghtk.Controller;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestService {
//    private static final String getPackageLogsById = "/admin/api/getPackageLogsById";
    private static final String BASE_URL = "https://admin.ghtk.vn/";
    public static Retrofit instance;

    public static Retrofit getInstance() {
        if (instance == null) {
            instance = new Retrofit.Builder().baseUrl(BASE_URL).client(new OkHttpClient.Builder().build()).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return instance;
    }

}
