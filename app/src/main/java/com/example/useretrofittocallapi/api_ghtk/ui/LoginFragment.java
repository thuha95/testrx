package com.example.useretrofittocallapi.api_ghtk.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.useretrofittocallapi.MainActivity;
import com.example.useretrofittocallapi.R;
import com.example.useretrofittocallapi.api_ghtk.Controller.RequestAPI;
import com.example.useretrofittocallapi.api_ghtk.Controller.RequestService;
import com.example.useretrofittocallapi.api_ghtk.model.User;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginFragment extends Fragment implements View.OnClickListener {
    private EditText edtUser, edtPass;
    private Retrofit retrofit = RequestService.getInstance();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frg_login, container, false);
        initView(v);
        return v;
    }

    private void initView(View v) {
        edtUser = v.findViewById(R.id.edt_user_name);
        edtPass = v.findViewById(R.id.edt_pass);

        v.findViewById(R.id.bt_login).setOnClickListener(this);
        v.findViewById(R.id.bt_register).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_login) {
            doLogin(edtUser.getText().toString(), edtPass.getText().toString());

        } else if (v.getId() == R.id.bt_register) {
            Toast.makeText(getContext(), "REGISTER", Toast.LENGTH_SHORT).show();
        }
    }

    private void doLogin(String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(getContext(), "Không để trống dữ liệu", Toast.LENGTH_SHORT).show();
            return;
        }
        String path = "http://chinhlv1.ghtklab.com/admin/login";

        String bod = "User[password]=" + password + "&" + "User[username]=" + username;
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/x-www-form-urlencoded; charset=utf-8"), bod);
        String cookie = "PHPSESSID=tb4ad295tncbcipferqe5lt6f5";

        getRequestAPT().doPostWithoutCookie(path, body).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.i("TAGGGG", "onResponse: " + response.body().getName());
                Log.i("TAGGGG", "onResponse: " + "Đăng nhập thành công");
                SearchFragment searchFrg = new SearchFragment();
                if (getMainActivity() != null) {
                    FragmentTransaction transaction = getMainActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, searchFrg, SearchFragment.class.getName());
                    transaction.commitAllowingStateLoss();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.i("TAGGGG", "onResponse: " + "Failllll");

            }
        });

    }

    public MainActivity getMainActivity() {
        if (getActivity() instanceof MainActivity) {
            return (MainActivity) getActivity();
        } else {
            return null;
        }
    }

    private RequestAPI getRequestAPT() {
        return retrofit.create(RequestAPI.class);
    }
}
