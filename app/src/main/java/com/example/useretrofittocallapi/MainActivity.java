package com.example.useretrofittocallapi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements OnActionCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        MenuFragment menuFrg = new MenuFragment();
        menuFrg.setmCallBack(this);
        showFrg(menuFrg, MenuFragment.class.getName(), false);

    }

    private void showFrg(Fragment fragment, String name, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, name);
        if(addToBackStack){
            transaction.addToBackStack(name);
        }
        transaction.commit();
    }


    @Override
    public void showFragment(Fragment fragment, String name) {
        showFrg(fragment, name, true);
    }
}
